'use strict';

const chai = require('chai');
const spies = require('chai-spies')
chai.use(spies)
const {
  spy,
  expect
} = chai;
const {
  createStore,
  combineReducers,
  applyMiddleware
} = require('redux');
const reduxHook = require('..');

const testReducer = combineReducers({
  testA: (state = '', action) => {
    switch (action.type) {
      case 'SET_A':
        return action.payload;
      case 'SET_ALL':
        return action.payload.a;
    }
    return state;
  },
  testB: (state = '', action) => {
    switch (action.type) {
      case 'SET_B':
        return action.payload;
      case 'SET_ALL':
        return action.payload.b;
    }
    return state;
  }
});

const createTestStore = () => {
  const {
    middleware,
    decorator
  } = reduxHook({
    ignoreSubscribersProp: 'mutex'
  });
  return decorator(createStore(testReducer, applyMiddleware(middleware)));
};

describe('redux hook middleware', () => {
  it('should ignore events with mutex', () => {
    const {
      subscribeToState,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToState(subscribeSpy);
    dispatch({
      type: 'SET_A',
      mutex: true,
      payload: 'test'
    });
    dispatch({
      type: 'SET_B',
      payload: 'test'
    });
    expect(subscribeSpy).to.have.been.called.once;
  });
  it('should subscribe to the state', () => {
    const {
      subscribeToState,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToState(subscribeSpy);
    dispatch({
      type: 'SET_A',
      payload: 'test'
    });
    dispatch({
      type: 'SET_B',
      payload: 'test'
    });
    expect(subscribeSpy).to.have.been.called.twice;
  });
  it('should subscribe once to the state', () => {
    const {
      subscribeToStateOnce,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToStateOnce(subscribeSpy);
    dispatch({
      type: 'SET_A',
      payload: 'test'
    });
    dispatch({
      type: 'SET_B',
      payload: 'test'
    });
    expect(subscribeSpy).to.have.been.called.once;
  });
  it('should subscribe to key changes', () => {
    const {
      subscribeToKey,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToKey('testA', subscribeSpy);
    dispatch({
      type: 'SET_A',
      payload: 'test'
    });
    dispatch({
      type: 'SET_B',
      payload: 'test'
    });
    expect(subscribeSpy).to.have.been.called.once;
  });
  it('should subscribe to events', () => {
    const {
      subscribeToEvent,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToEvent('SET_A', subscribeSpy);
    dispatch({
      type: 'SET_A',
      payload: 'test'
    });
    dispatch({
      type: 'SET_B',
      payload: 'test'
    });
    expect(subscribeSpy).to.have.been.called.once;
  });
  it('should subscribe to mixed', () => {
    const {
      subscribeToMixed,
      dispatch
    } = createTestStore();
    const subscribeSpy = spy(() => {});
    subscribeToMixed([{ event: ['SET_A'] }, { key: 'testB' }], subscribeSpy);
    dispatch({
      type: 'SET_A',
      payload: ''
    });
    dispatch({
      type: 'SET_B',
      payload: 'b'
    });
    dispatch({
      type: 'SET_B',
      payload: 'b'
    });
    expect(subscribeSpy).to.have.been.called.twice;
  });
});
