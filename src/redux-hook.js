'use strict';

import assign from 'object-assign';
import findIndex from 'array-find-index';
import find from 'array-find';
import isArray from 'isarray';
import reduce from 'array-reduce';
import keys from 'object-keys';
import forEach from 'array-foreach';

const [
  EVENT,
  KEY,
  STATE,
  MIXED
] = [
  { evt: true },
  { key: true },
  { state: true },
  { mixed: true }
];

export default ({
  ignoreSubscribersProp
} = {}) => {
  const subscribers = [];
  const addSubscriber = (s) => {
    subscribers.push(s);
    return () => {
      const i = findIndex(subscribers, (v) => v === s);
      if (~i) {
        subscribers.splice(i, 1);
        return true;
      }
      return false;
    };
  };
  const baseDecoratorProps = {
    subscribeToEvent: (evt, fn) => addSubscriber({
      type: EVENT,
      evt,
      fn
    }),
    subscribeToKey: (key, fn) => addSubscriber({
      type: KEY,
      key,
      fn
    }),
    subscribeToState: (fn) => addSubscriber({
      type: STATE,
      fn
    }),
    subscribeToMixed: (mixed, fn) => addSubscriber({
      type: MIXED,
      mixed,
      fn
    })
  };
  const decoratorProps = assign(baseDecoratorProps, reduce(keys(baseDecoratorProps), (r, v) => {
    const decoratedName = v + 'Once';
    const call = baseDecoratorProps[v];
    r[decoratedName] = (...args) => {
      const [ fn ] = args.splice(args.length - 1, 1);
      const unsubscribe = call(...[ ...args, (...params) => {
        unsubscribe();
        fn(...params);
      } ]);
      return unsubscribe;
    };
    return r;
  }, {}));
  const decorator = (store) => assign(store, decoratorProps);
  return assign({
    middleware: ({ getState }) => (next) => (action) => {
      const lastState = getState();
      next(action);
      const nextState = getState();
      if (action[ignoreSubscribersProp]) return;
      forEach(subscribers, ({
        type,
        evt,
        fn,
        key,
        mixed
      }) => {
        switch (type) {
          case STATE:
            return fn(action, nextState, lastState);
          case KEY:
            if (isArray(key) ? find(key, (v) => nextState[v] !== lastState[v]) : nextState[key] !== lastState[key]) return fn(action, nextState, lastState);
            break;
          case EVENT:
            if (isArray(evt) ? find(evt, (v) => v === action.type) : evt === action.type) return fn(action, nextState, lastState);
            break;
          case MIXED:
            if (find(isArray(mixed) ? mixed : [ mixed ], ({ event, key }) => event ? (isArray(event) ? find(event, (v) => v === action.type) : event === action.type) : (isArray(key) ? find(key, (v) => lastState[v] !== nextState[v]) : lastState[key] !== nextState[key]))) return fn(action, nextState, lastState);
            break;
        }
      });
    },
    decorator
  }, decoratorProps);
};
