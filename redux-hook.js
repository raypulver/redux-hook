(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("reduxHook", [], factory);
	else if(typeof exports === 'object')
		exports["reduxHook"] = factory();
	else
		root["reduxHook"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	var _objectAssign = __webpack_require__(1);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _arrayFindIndex = __webpack_require__(2);
	
	var _arrayFindIndex2 = _interopRequireDefault(_arrayFindIndex);
	
	var _arrayFind = __webpack_require__(3);
	
	var _arrayFind2 = _interopRequireDefault(_arrayFind);
	
	var _isarray = __webpack_require__(4);
	
	var _isarray2 = _interopRequireDefault(_isarray);
	
	var _arrayReduce = __webpack_require__(5);
	
	var _arrayReduce2 = _interopRequireDefault(_arrayReduce);
	
	var _objectKeys = __webpack_require__(6);
	
	var _objectKeys2 = _interopRequireDefault(_objectKeys);
	
	var _arrayForeach = __webpack_require__(8);
	
	var _arrayForeach2 = _interopRequireDefault(_arrayForeach);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var EVENT = { evt: true },
	    KEY = { key: true },
	    STATE = { state: true },
	    MIXED = { mixed: true };
	
	exports.default = function () {
	  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
	      ignoreSubscribersProp = _ref.ignoreSubscribersProp;
	
	  var subscribers = [];
	  var addSubscriber = function addSubscriber(s) {
	    subscribers.push(s);
	    return function () {
	      var i = (0, _arrayFindIndex2.default)(subscribers, function (v) {
	        return v === s;
	      });
	      if (~i) {
	        subscribers.splice(i, 1);
	        return true;
	      }
	      return false;
	    };
	  };
	  var baseDecoratorProps = {
	    subscribeToEvent: function subscribeToEvent(evt, fn) {
	      return addSubscriber({
	        type: EVENT,
	        evt: evt,
	        fn: fn
	      });
	    },
	    subscribeToKey: function subscribeToKey(key, fn) {
	      return addSubscriber({
	        type: KEY,
	        key: key,
	        fn: fn
	      });
	    },
	    subscribeToState: function subscribeToState(fn) {
	      return addSubscriber({
	        type: STATE,
	        fn: fn
	      });
	    },
	    subscribeToMixed: function subscribeToMixed(mixed, fn) {
	      return addSubscriber({
	        type: MIXED,
	        mixed: mixed,
	        fn: fn
	      });
	    }
	  };
	  var decoratorProps = (0, _objectAssign2.default)(baseDecoratorProps, (0, _arrayReduce2.default)((0, _objectKeys2.default)(baseDecoratorProps), function (r, v) {
	    var decoratedName = v + 'Once';
	    var call = baseDecoratorProps[v];
	    r[decoratedName] = function () {
	      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	        args[_key] = arguments[_key];
	      }
	
	      var _args$splice = args.splice(args.length - 1, 1),
	          _args$splice2 = _slicedToArray(_args$splice, 1),
	          fn = _args$splice2[0];
	
	      var unsubscribe = call.apply(undefined, [].concat(args, [function () {
	        unsubscribe();
	        fn.apply(undefined, arguments);
	      }]));
	      return unsubscribe;
	    };
	    return r;
	  }, {}));
	  var decorator = function decorator(store) {
	    return (0, _objectAssign2.default)(store, decoratorProps);
	  };
	  return (0, _objectAssign2.default)({
	    middleware: function middleware(_ref2) {
	      var getState = _ref2.getState;
	      return function (next) {
	        return function (action) {
	          var lastState = getState();
	          next(action);
	          var nextState = getState();
	          if (action[ignoreSubscribersProp]) return;
	          (0, _arrayForeach2.default)(subscribers, function (_ref3) {
	            var type = _ref3.type,
	                evt = _ref3.evt,
	                fn = _ref3.fn,
	                key = _ref3.key,
	                mixed = _ref3.mixed;
	
	            switch (type) {
	              case STATE:
	                return fn(action, nextState, lastState);
	              case KEY:
	                if ((0, _isarray2.default)(key) ? (0, _arrayFind2.default)(key, function (v) {
	                  return nextState[v] !== lastState[v];
	                }) : nextState[key] !== lastState[key]) return fn(action, nextState, lastState);
	                break;
	              case EVENT:
	                if ((0, _isarray2.default)(evt) ? (0, _arrayFind2.default)(evt, function (v) {
	                  return v === action.type;
	                }) : evt === action.type) return fn(action, nextState, lastState);
	                break;
	              case MIXED:
	                if ((0, _arrayFind2.default)((0, _isarray2.default)(mixed) ? mixed : [mixed], function (_ref4) {
	                  var event = _ref4.event,
	                      key = _ref4.key;
	                  return event ? (0, _isarray2.default)(event) ? (0, _arrayFind2.default)(event, function (v) {
	                    return v === action.type;
	                  }) : event === action.type : (0, _isarray2.default)(key) ? (0, _arrayFind2.default)(key, function (v) {
	                    return lastState[v] !== nextState[v];
	                  }) : lastState[key] !== nextState[key];
	                })) return fn(action, nextState, lastState);
	                break;
	            }
	          });
	        };
	      };
	    },
	    decorator: decorator
	  }, decoratorProps);
	};
	
	module.exports = exports['default'];

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	/*
	object-assign
	(c) Sindre Sorhus
	@license MIT
	*/
	
	'use strict';
	/* eslint-disable no-unused-vars */
	var getOwnPropertySymbols = Object.getOwnPropertySymbols;
	var hasOwnProperty = Object.prototype.hasOwnProperty;
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;
	
	function toObject(val) {
		if (val === null || val === undefined) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}
	
		return Object(val);
	}
	
	function shouldUseNative() {
		try {
			if (!Object.assign) {
				return false;
			}
	
			// Detect buggy property enumeration order in older V8 versions.
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=4118
			var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
			test1[5] = 'de';
			if (Object.getOwnPropertyNames(test1)[0] === '5') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test2 = {};
			for (var i = 0; i < 10; i++) {
				test2['_' + String.fromCharCode(i)] = i;
			}
			var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
				return test2[n];
			});
			if (order2.join('') !== '0123456789') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test3 = {};
			'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
				test3[letter] = letter;
			});
			if (Object.keys(Object.assign({}, test3)).join('') !==
					'abcdefghijklmnopqrst') {
				return false;
			}
	
			return true;
		} catch (err) {
			// We don't expect any of the above to throw, but better to be safe.
			return false;
		}
	}
	
	module.exports = shouldUseNative() ? Object.assign : function (target, source) {
		var from;
		var to = toObject(target);
		var symbols;
	
		for (var s = 1; s < arguments.length; s++) {
			from = Object(arguments[s]);
	
			for (var key in from) {
				if (hasOwnProperty.call(from, key)) {
					to[key] = from[key];
				}
			}
	
			if (getOwnPropertySymbols) {
				symbols = getOwnPropertySymbols(from);
				for (var i = 0; i < symbols.length; i++) {
					if (propIsEnumerable.call(from, symbols[i])) {
						to[symbols[i]] = from[symbols[i]];
					}
				}
			}
		}
	
		return to;
	};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

	'use strict';
	module.exports = function (arr, predicate, ctx) {
		if (typeof Array.prototype.findIndex === 'function') {
			return arr.findIndex(predicate, ctx);
		}
	
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
	
		var list = Object(arr);
		var len = list.length;
	
		if (len === 0) {
			return -1;
		}
	
		for (var i = 0; i < len; i++) {
			if (predicate.call(ctx, list[i], i, list)) {
				return i;
			}
		}
	
		return -1;
	};


/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';
	
	function find(array, predicate, context) {
	  if (typeof Array.prototype.find === 'function') {
	    return array.find(predicate, context);
	  }
	
	  context = context || this;
	  var length = array.length;
	  var i;
	
	  if (typeof predicate !== 'function') {
	    throw new TypeError(predicate + ' is not a function');
	  }
	
	  for (i = 0; i < length; i++) {
	    if (predicate.call(context, array[i], i, array)) {
	      return array[i];
	    }
	  }
	}
	
	module.exports = find;


/***/ }),
/* 4 */
/***/ (function(module, exports) {

	var toString = {}.toString;
	
	module.exports = Array.isArray || function (arr) {
	  return toString.call(arr) == '[object Array]';
	};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	var hasOwn = Object.prototype.hasOwnProperty;
	
	module.exports = function (xs, f, acc) {
	    var hasAcc = arguments.length >= 3;
	    if (hasAcc && xs.reduce) return xs.reduce(f, acc);
	    if (xs.reduce) return xs.reduce(f);
	    
	    for (var i = 0; i < xs.length; i++) {
	        if (!hasOwn.call(xs, i)) continue;
	        if (!hasAcc) {
	            acc = xs[i];
	            hasAcc = true;
	            continue;
	        }
	        acc = f(acc, xs[i], i);
	    }
	    return acc;
	};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	// modified from https://github.com/es-shims/es5-shim
	var has = Object.prototype.hasOwnProperty;
	var toStr = Object.prototype.toString;
	var slice = Array.prototype.slice;
	var isArgs = __webpack_require__(7);
	var isEnumerable = Object.prototype.propertyIsEnumerable;
	var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
	var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
	var dontEnums = [
		'toString',
		'toLocaleString',
		'valueOf',
		'hasOwnProperty',
		'isPrototypeOf',
		'propertyIsEnumerable',
		'constructor'
	];
	var equalsConstructorPrototype = function (o) {
		var ctor = o.constructor;
		return ctor && ctor.prototype === o;
	};
	var excludedKeys = {
		$console: true,
		$external: true,
		$frame: true,
		$frameElement: true,
		$frames: true,
		$innerHeight: true,
		$innerWidth: true,
		$outerHeight: true,
		$outerWidth: true,
		$pageXOffset: true,
		$pageYOffset: true,
		$parent: true,
		$scrollLeft: true,
		$scrollTop: true,
		$scrollX: true,
		$scrollY: true,
		$self: true,
		$webkitIndexedDB: true,
		$webkitStorageInfo: true,
		$window: true
	};
	var hasAutomationEqualityBug = (function () {
		/* global window */
		if (typeof window === 'undefined') { return false; }
		for (var k in window) {
			try {
				if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
					try {
						equalsConstructorPrototype(window[k]);
					} catch (e) {
						return true;
					}
				}
			} catch (e) {
				return true;
			}
		}
		return false;
	}());
	var equalsConstructorPrototypeIfNotBuggy = function (o) {
		/* global window */
		if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
			return equalsConstructorPrototype(o);
		}
		try {
			return equalsConstructorPrototype(o);
		} catch (e) {
			return false;
		}
	};
	
	var keysShim = function keys(object) {
		var isObject = object !== null && typeof object === 'object';
		var isFunction = toStr.call(object) === '[object Function]';
		var isArguments = isArgs(object);
		var isString = isObject && toStr.call(object) === '[object String]';
		var theKeys = [];
	
		if (!isObject && !isFunction && !isArguments) {
			throw new TypeError('Object.keys called on a non-object');
		}
	
		var skipProto = hasProtoEnumBug && isFunction;
		if (isString && object.length > 0 && !has.call(object, 0)) {
			for (var i = 0; i < object.length; ++i) {
				theKeys.push(String(i));
			}
		}
	
		if (isArguments && object.length > 0) {
			for (var j = 0; j < object.length; ++j) {
				theKeys.push(String(j));
			}
		} else {
			for (var name in object) {
				if (!(skipProto && name === 'prototype') && has.call(object, name)) {
					theKeys.push(String(name));
				}
			}
		}
	
		if (hasDontEnumBug) {
			var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);
	
			for (var k = 0; k < dontEnums.length; ++k) {
				if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
					theKeys.push(dontEnums[k]);
				}
			}
		}
		return theKeys;
	};
	
	keysShim.shim = function shimObjectKeys() {
		if (Object.keys) {
			var keysWorksWithArguments = (function () {
				// Safari 5.0 bug
				return (Object.keys(arguments) || '').length === 2;
			}(1, 2));
			if (!keysWorksWithArguments) {
				var originalKeys = Object.keys;
				Object.keys = function keys(object) {
					if (isArgs(object)) {
						return originalKeys(slice.call(object));
					} else {
						return originalKeys(object);
					}
				};
			}
		} else {
			Object.keys = keysShim;
		}
		return Object.keys || keysShim;
	};
	
	module.exports = keysShim;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';
	
	var toStr = Object.prototype.toString;
	
	module.exports = function isArguments(value) {
		var str = toStr.call(value);
		var isArgs = str === '[object Arguments]';
		if (!isArgs) {
			isArgs = str !== '[object Array]' &&
				value !== null &&
				typeof value === 'object' &&
				typeof value.length === 'number' &&
				value.length >= 0 &&
				toStr.call(value.callee) === '[object Function]';
		}
		return isArgs;
	};


/***/ }),
/* 8 */
/***/ (function(module, exports) {

	/**
	 * array-foreach
	 *   Array#forEach ponyfill for older browsers
	 *   (Ponyfill: A polyfill that doesn't overwrite the native method)
	 * 
	 * https://github.com/twada/array-foreach
	 *
	 * Copyright (c) 2015-2016 Takuto Wada
	 * Licensed under the MIT license.
	 *   https://github.com/twada/array-foreach/blob/master/MIT-LICENSE
	 */
	'use strict';
	
	module.exports = function forEach (ary, callback, thisArg) {
	    if (ary.forEach) {
	        ary.forEach(callback, thisArg);
	        return;
	    }
	    for (var i = 0; i < ary.length; i+=1) {
	        callback.call(thisArg, ary[i], i, ary);
	    }
	};


/***/ })
/******/ ])
});
;
//# sourceMappingURL=redux-hook.js.map